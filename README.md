# Demonstration project for bus tracking

This project is a protoype of bus tracking system. It only works on bus 672 in Taipei City.


使用方法：
執行 main.py，待Server啟動後即可使用 Http傳送指令。
*注意：TDC/SMTP 金鑰已從中移除，故程式無法直接運行

可操作指令如下：
1. -GET http://127.0.0.1:6000
return: 672 路線 Raw data
[{'StopUID': 'TPE38942', 'StopID': '38942', 'StopName': {'Zh_tw': '中正環河路口', 'En': 'Zhongzheng&Huanhe Rd.Intersection'},.........]

2. -GET http://127.0.0.1:6000/st672
return: 路線672公車當前的各站到站時間(整理後)
[
{
        "Direction": "Dapeng New Village",
        "EstimateTime": 1081,
        "RouteName": "672",
        "StopName": "Zhongzheng&Huanhe Rd.Intersection",
        "StopStatus": 0
},
......
]

3. -POST http://127.0.0.1:6000/register
body: 
{
"email":"555@gmail.com",
"route":"207",
"direction":"1",
"stop":"MRT Houshanpi Sta.(Zhongxiao)"
}
return: {"return": "data accepted"} / reject (僅確認json格式，無其他防呆機制)

![get_raw](./img/get_raw.png)

![get_672](./img/get_672.png)

![post_stop](./img/post_regis_stop.png)


Current system structure
![20221109_134936698_iOS.png](./img/20221109_134936698_iOS.png)


better system structure
![20221109_134856268_iOS.png](./img/20221109_134856268_iOS.png)
