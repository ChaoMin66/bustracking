# 郵件通知功能

import smtplib

registered_stops = []

class Email_Notify:

    def __init__(self):
        self.smtp_object = smtplib.SMTP('smtp.gmail.com', 587)
        self.smtp_object.ehlo()
        self.smtp_object.starttls()
        self.ServerEmail = 'my_personal_email'
        self.ServerPassword = 'my_application_password'
        self.smtp_object.login(self.ServerEmail, self.ServerPassword)

    def send_email_notification(self, route, direction, stop, email_lists):
        for email in email_lists:
            from_address = self.ServerEmail
            to_address = email
            subject = 'bus will arrive soon'
            message = f'{route}, {direction}, {stop}'
            msg = "Subject: " + subject + '\n' + message
            self.smtp_object.sendmail(from_address, to_address, msg)

    def end(self):
        self.smtp_object.quit()



