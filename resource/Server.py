'''
In this module, server keep querying transport data from https://ptx.transportdata.tw/PTX/Service
'''

import requests
import json
import time


# here demonstrates only with route 672

DepaDestUrl = "https://tdx.transportdata.tw/api/basic/v2/Bus/Route/City/Taipei/672?%24format=JSON"
ArrivalTimeUrl = "https://tdx.transportdata.tw/api/basic/v2/Bus/EstimatedTimeOfArrival/City/Taipei/672?%24format=JSON"


class Data:

    def __init__(self, app_id, app_key):
        self.auth_response = None
        self.app_id = app_id
        self.app_key = app_key
        self.pulling = True
        self.data_response_672 = None
        self.direction_info_672 = {}

    def set_auth_response(self, auth_response):
        self.auth_response = auth_response

    def get_data_header(self):
        auth_JSON = json.loads(self.auth_response.text)
        access_token = auth_JSON.get('access_token')

        return{
            'authorization': 'Bearer '+access_token
        }

    def get_response(self):
        return self.data_response_672

    def get_direction_info_672(self):
        return self.direction_info_672

    def TDC_direction_info_672(self):
        r = requests.get(DepaDestUrl, headers=self.get_data_header())
        r_json = r.json()
        for ele in r_json:
            if ele['RouteName']['En'] == '672':
                self.direction_info_672['0'] = ele['DestinationStopNameEn']
                self.direction_info_672['1'] = ele['DepartureStopNameEn']

    def TDC_data_pulling_672(self):
        while self.pulling:
            r = requests.get(ArrivalTimeUrl, headers=self.get_data_header())
            self.data_response_672 = r.json()

            time.sleep(60)


