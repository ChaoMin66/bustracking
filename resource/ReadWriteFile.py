# 讀寫註冊記錄資訊

import json

class FileIO:
    def __init__(self):
        self.encoding = "utf-8"
        self.filelocation = 'Regis/RegisteredStops.json'
        self.registeredData = None

    # 讀已經過註冊的資訊
    def readfile(self):
        # Opening JSON file, returns JSON object as a dictionary
        f = open(self.filelocation, encoding=self.encoding, mode='r')
        self.registeredData = json.load(f)
        f.close()
        return self.registeredData

    # 寫入使用者註冊的請求
    def writefile(self, route, direction, stop, email):
        if route in self.registeredData:
            if direction in self.registeredData[route]:
                if stop in self.registeredData[route][direction]:
                    if email in self.registeredData[route][direction][stop]:
                        pass
                    else:
                        self.registeredData[route][direction][stop].append(email)
                else:
                    self.registeredData[route][direction][stop] = [email]
            else:
                self.registeredData[route][direction] = {stop: [email]}
        else:
            self.registeredData[route] = {direction: {stop: [email]}}

        out_file = open(self.filelocation, encoding=self.encoding, mode='w')
        json.dump(self.registeredData, out_file, indent=6)
        out_file.close()
        return self.registeredData
