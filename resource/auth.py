# 註冊 TDC 資訊的API使用權限

import requests


class Auth:
    def __init__(self):
        self.app_id = 'my_app_id'
        self.app_key = 'my_app_key'
        self.auth_url = "https://tdx.transportdata.tw/auth/realms/TDXConnect/protocol/openid-connect/token"

    def get_app_id(self):
        return self.app_id

    def get_app_key(self):
        return self.app_key

    def get_auth(self):
        content_type = 'application/x-www-form-urlencoded'
        grant_type = 'client_credentials'

        auth_header = {
            'content-type' : content_type,
            'grant_type' : grant_type,
            'client_id' : self.app_id,
            'client_secret' : self.app_key
        }
        auth_response = requests.post(self.auth_url, auth_header)
        return auth_response



