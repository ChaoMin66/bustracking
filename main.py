import threading
import time
from flask import Flask, request

from resource.auth import Auth
from resource.Server import Data
from resource.Notify import Email_Notify
from resource.ReadWriteFile import FileIO

# 確認註冊的站牌位置與信箱
def checkRegisteredStops():
    registered_stops = f.readfile()
    data_response = data.get_response()
    for reg in registered_stops:
        for d in registered_stops[reg]:
            for stp in registered_stops[reg][d]:
                for st in data_response:
                    route_name = st['RouteName']['En']
                    stop_name = st['StopName']['En']
                    stop_status = st['StopStatus']
                    direction = st['Direction']
                    if stop_status == 0:
                        estimate_time = st['EstimateTime']

                    if stp == stop_name and d == str(direction) and reg == route_name and stop_status == 0:
                        if 300 > estimate_time > 180:
                            N.send_email_notification(reg, d, stp, registered_stops[reg][d][stp])

    time.sleep(120)
    return checkRegisteredStops()


# -----APIs----------------------------------------------------------------------

app = Flask(__name__)

# 回傳所有 672 資訊的 Raw Data
@app.route("/")
def get_response_raw():
    data_response = data.get_response()
    return f"{data_response}"

# 回傳使用者 672 所有站牌到站時間資訊
# -Get http://127.0.0.1:6000/st672
@app.route("/st672")
def get_response_672():
    data_response = data.get_response()
    st672_list = []
    for st672 in data_response:
        route_name = st672['RouteName']['En']
        stop_name = st672['StopName']['En']
        stop_status = st672['StopStatus']
        direction = st672['Direction']
        if stop_status == 0:
            estimate_time = st672['EstimateTime']
        else:
            estimate_time = -1
        direction_info = data.get_direction_info_672()
        if direction == 1:
            direction_text = direction_info['1']
        else:
            direction_text = direction_info['0']
        st672_list.append({"RouteName": route_name, "StopName": stop_name, "Direction": direction_text,
                           "StopStatus": stop_status, "EstimateTime": estimate_time})
    return st672_list


# accept post body: (尚未做錯誤處理)
# -POST http://127.0.0.1:6000/register
# {
#     "email":"123@gmail.com",
#     "route":"672",
#     "direction":"1",
#     "stop":"Jingjian New Village"
# }
@app.route("/register", methods=['POST'])
def stop_register():
    content_type = request.headers.get('Content-Type')
    if content_type == 'application/json':
        jsondata = request.json
        reg_route = jsondata['route']
        reg_direction = jsondata['direction']
        reg_stop = jsondata['stop']
        reg_email = jsondata['email']
        f.writefile(reg_route, reg_direction, reg_stop, reg_email)
        return '{"return": "data accepted"}'
    else:
        return 'Content-Type not supported!'

# 無刪除註冊功能


# -----Main----------------------------------------------------------------------

if __name__ == "__main__":
    # Auth
    a = Auth()
    auth_response = a.get_auth()
    app_id = a.get_app_id()
    app_key = a.get_app_key()

    data = Data(app_id, app_key)
    data.set_auth_response(auth_response)
    data.TDC_direction_info_672()
    # data_response_672 = d.get_data()

    # 讀檔案內容
    f = FileIO()
    # email 功能
    N = Email_Notify()

    # Threading
    t1 = threading.Thread(target=data.TDC_data_pulling_672)
    t1.setDaemon(True)
    t1.start()
    time.sleep(5)
    t2 = threading.Thread(target=checkRegisteredStops)
    t2.setDaemon(True)
    t2.start()

    app.run(port=6000, debug=False)